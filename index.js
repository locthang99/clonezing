const crypto = require("crypto");

const getHash256 = (a) => {
  return crypto.createHash("sha256").update(a).digest("hex");
};
const getHmac512 = (str, key) => {
  let hmac = crypto.createHmac("sha512", key);
  return hmac.update(Buffer.from(str, "utf8")).digest("hex");
};
//console.log(getHash256("ctime=161141362id=ZO67W0EO"))
//console.log(getHmac512("/api/v2/song/getInfo"+getHash256("ctime=1611413628id=ZWZCF0CDversion=1.0.19"),"882QcNXV4tUZbvAsjmFOHqNC1LpcBRKW"))
//process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 1
const fetch = require("node-fetch");
var id = "ZOZ0WD80";
var ctime = "1611413629";
var p = "/api/v2/song/getInfo";
var path = "/api/v2/song/getStreaming";
var pathLyric = "/api/v2/lyric";
var sig = getHmac512(
  path + getHash256("ctime=" + ctime + "id=" + id + "version=1.0.19"),
  "882QcNXV4tUZbvAsjmFOHqNC1LpcBRKW"
);
var url =
  "https://zingmp3.vn/api/v2/song/getInfo?id=" +
  id +
  "&ctime=" +
  ctime +
  "&version=1.0.19&sig=" +
  sig +
  "&apiKey=kI44ARvPwaqL7v0KuDSM0rGORtdY1nnw";
function getUrl(id, ctime) {
  var sig = getHmac512(
    p + getHash256("ctime=" + ctime + "id=" + id + "version=1.0.19"),
    "882QcNXV4tUZbvAsjmFOHqNC1LpcBRKW"
  );
  //console.log('https://zingmp3.vn/api/v2/song/getStreaming?id='+id+'&ctime='+ctime+'&version=1.0.19&sig='+sig+'&apiKey=kI44ARvPwaqL7v0KuDSM0rGORtdY1nnw');
  return (
    "https://zingmp3.vn/api/v2/song/getInfo?id=" +
    id +
    "&ctime=" +
    ctime +
    "&version=1.0.19&sig=" +
    sig +
    "&apiKey=kI44ARvPwaqL7v0KuDSM0rGORtdY1nnw"
  );
}
function getLyric(id, ctime) {
  var sig = getHmac512(
    pathLyric + getHash256("ctime=" + ctime + "id=" + id + "version=1.0.22"),
    "882QcNXV4tUZbvAsjmFOHqNC1LpcBRKW"
  );
  //console.log('https://zingmp3.vn/api/v2/song/getStreaming?id='+id+'&ctime='+ctime+'&version=1.0.19&sig='+sig+'&apiKey=kI44ARvPwaqL7v0KuDSM0rGORtdY1nnw');
  return (
    "https://zingmp3.vn/api/v2/lyric?id=" +
    id +
    "&ctime=" +
    ctime +
    "&version=1.0.22&sig=" +
    sig +
    "&apiKey=kI44ARvPwaqL7v0KuDSM0rGORtdY1nnw"
  );
}
function getId(num) {
  var t = num.toString(21).toUpperCase();
  //console.log(t)
  t = t
    .replace(/[I]/g, "U") //18
    .replace(/[G]/g, "I") //16
    .replace(/[H]/g, "O") //17
    .replace(/[J]/g, "W") //19
    .replace(/[K]/g, "Z");//20
  return t;
}

const fs = require("fs");
const { throws } = require("assert");
const { exception } = require("console");
function writeData(path, data) {
  fs.appendFile(path, "\n" + data + ",", function (err) {
    if (err) writeErrorWrite(data);
  });
}

function writeErrorWrite(err) {
  fs.appendFile("Error/logWrite.txt", "\n" + err, function (err) {
    if (err) throw err;
  });
}
function writeError(err) {
  fs.appendFile("Error/log.txt", "\n" + err, function (err) {
    if (err) throw err;
  });
}

function writeErrorLogic(err) {
  fs.appendFile("Error/logLogic.txt", "\n" + err, function (err) {
    if (err) throw err;
  });
}
var cookie = '';
function timeout(ms, promise) {
  return new Promise((resolve, reject) => {
    const timer = setTimeout(() => {
      reject(new Error('TIMEOUT'))
    }, ms)

    promise
      .then(value => {
        clearTimeout(timer)
        resolve(value)
      })
      .catch(reason => {
        clearTimeout(timer)
        reject(reason)
      })
  })
}
async function callApi(url) {
  return await fetch(url, {
    method: 'GET',
    //agent: new HttpsProxyAgent("http://proxy.tma.com.vn:8080"),
    headers: {
      cookie: cookie
    },
  })
    .then((res) =>  res.json())
    .then((res) => {
      try{
      if (res.err == 0) {
        if (!res.data.isRBT) {
          let obj = resolveObj(res.data);
          if (obj.types.includes("viet-nam")) {
            writeData("Cover/VN/" + obj.types, JSON.stringify(obj));
          } else {
            writeData("Cover/NotVN/" + obj.types, JSON.stringify(obj));
          }
        } else {
          let obj = resolveObj(res.data);
          if (obj.types.includes("viet-nam")) {
            writeData("Official/VN/" + obj.types, JSON.stringify(obj));
          } else {
            writeData("Official/NotVN/" + obj.types, JSON.stringify(obj));
          }
        }
      } else {
        if (res.err != -1023) {
          console.log(res);
          writeError(url);
          throw "vcl";
        } else {
          //console.log("Not found")
        }
      }
      return "";
    }
    catch(err)
    {
      if(err =='vcl')
      throw 'vcl'
      writeErrorLogic(url);
      
    }
    }).catch(err =>{
      if(err=='vcl')
      {
        console.log('cookies expire')
        throw 'vcl'
      }
      console.log(err)
      writeError(url)
    })
      
}

async function fetchUrls(start, num) {
  writeData("total.txt",start)
  console.log("------------------- \n start " + start);
  Log();
  let listCall = [];
  for (let i = start; i > start - num; i--) {
    listCall.push(timeout(10000,callApi(getUrl("ZW" + getId(i), "1615448655"))).catch(err=>{
      if(err=='vcl')
      throw err;
      console.log(err)
      writeError(url)


    }));
    listCall.push(timeout(10000,callApi(getUrl("ZO" + getId(i), "1615448655"))).catch(err=>{
      if(err=='vcl')
      throw err;
      console.log(err)
      writeError(url)


    }));
    //listCall.push(callApi(getUrl('IW'+getId(i),'1615448655')));
  }

  await Promise.all(listCall);

  console.log("------------------- \n end " + (start - num));
  Log();
}

//fetchUrls()
//console.log(getId(85766120))




function resolveObj(obj) {
  delete obj.isOffical;
  delete obj.username;
  delete obj.isWorldWide;
  delete obj.link;
  delete obj.isZMA;
  delete obj.zingChoise;
  delete obj.isPrivate;
  delete obj.preRelease;
  delete obj.streamingStatus;
  delete obj.allowAudioAds;
  delete obj.userid;
  delete obj.isRBT;
  delete obj.album;
  delete obj.radio;
  delete obj.liked;
  delete obj.mvlink;
  delete obj.alias;
  delete obj.thumbnail;

  //sections
  try {
    if (obj.sections[0]) {
      let listIdsections = [];
      obj.sections[0].items.forEach((element) => {
        listIdsections.push(element.encodeId);
      });
      obj.sections = listIdsections;
    }
  } catch (err) {
    console.log(err);
  }

  //artist
  try {
    if (obj.artists) {
      let listIdArtists = [];
      obj.artists.forEach((element) => {
        listIdArtists.push({ id: element.id, name: element.name });
        writeData(
          "Art/art.txt",
          JSON.stringify({ id: element.id, name: element.name })
        );
      });
      obj.artists = listIdArtists;
    }
  } catch (err) {
    console.log(err);
  }

  //composer
  try {
    if (obj.composers) {
      let listIdComposers = [];
      obj.composers.forEach((element) => {
        listIdComposers.push({ id: element.id, name: element.name });
      });
      obj.composers = listIdComposers;
    }
  } catch (err) {
    console.log(err);
  }
  try {
    //genres
    if (obj.genres) {
      let genres = [];
      let types = "";
      obj.genres.forEach((element) => {
        types += element.alias + "-";
        genres.push({ id: element.encodeId, name: element.name });
      });
      obj.genres = genres;
      obj.types = types;
    }
  } catch (err) {
    console.log(err);
  }
  return obj;
}

var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
async function Clone() {
  
  var start = 0
  var num =0
  var end =99999999999999999999999;
  cookie = ''

  // fetch("https://604f32afc20143001744c8aa.mockapi.io/api/v1/config/cf").then(res=>res.json())
  // .then(res=>{
  //   console.log(res)
  //   start = res.start;
  //   end =start - 10000000;
  //   num = res.num;
  //   cookie = res.cookie;
  // }).then(res=>{
  //   for (let i = start; i > end; i -= num) {
  //     await fetchUrls(i, num);
  //   }
  // })

  var resp = ""
  var xhr = new XMLHttpRequest();
  xhr.withCredentials = true;
  xhr.open("GET", "https://604f32afc20143001744c8aa.mockapi.io/api/v1/config/cf", false);
  xhr.send();
  if (xhr.readyState === 4) {
       resp = xhr.responseText
  }
  var res = JSON.parse(resp)
  start = res.start;
  end =start - 10000000;
  num = res.num;
  cookie = res.cookie;
  console.log(res)
    for (let i = start; i > end; i -= num) {
      await fetchUrls(i, num);
    }

}

function Log()
{
  let d =new Date();
  let time = d.getDate() +"/"+d.getMonth()+"/"+d.getFullYear() +"--"+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds()
  console.log(time)
}

Clone().catch(err=>{
  console.log(err)
  throw err
});
//     84757109
//fetchUrls(85766120,1)
//console.log(getUrl("ZWABCDAC","1615448655"))
//callApi("http://zingmp3.vn/api/v2/song/getInfo?id=ZO97OID8&ctime=1615473854&version=1.1.0&sig=51863a7122cd0879bc889f09b7c8a4e74a724117e37d4d3a439b20018eaa892ef01223cdd875c85f26742c75248ea258d85aa0b21a75be535cc384cbcb362e6a&apiKey=kI44ARvPwaqL7v0KuDSM0rGORtdY1nnw")
//console.log(getId(4084100))
//fetch("https://pokeapi.co/api/v2/pokemon/1").then(r=>r.json()).then(res=>console.log(res))
//callApi("https://pokeapi.co/api/v2/pokemon/1")
//timeout(100,fetch("https://192.168.34.5/api/v1"))
//fetch("https://192.168.34.5/api/v1")